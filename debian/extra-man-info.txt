[DESCRIPTION]

This command line tool uses SSW, a fast SIMD optimized alignment library,
to perform alignments on input files.

The input files can be in FASTA or FASTQ format. Both target and query files
can contain multiple sequences. Each sequence in the query file will be
aligned with all sequences in the target file. If the target file has N
sequences and the query file has M sequences, the results will have M*N alignments.

[OUTPUT FORMATS]

The software can output SAM format or BLAST like format results.
For a specification of the SAM format, please see
http://samtools.sourceforge.net/SAM1.pdf. The additional optional field
"ZS" indicates the suboptimal alignment score. 
